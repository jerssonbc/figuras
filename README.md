# figuras

API Rest para para el registro y obtención de datos de figuras geométricas (cuadrado, triángulo, círculo) y sus distintos parámetros (superficie; base; altura; diámetro; tipo de figura geométrica).

## Consideraciones

El proyecto se construyo con Apache NetBeans IDE 12.0 y usa las siguientes tecnologías:

- Java 11
- Spring Boot Version: 2.5.4
- Spring Web
- Spring Data JPA
- H2 Database (In-Memory)
- Maven
- lombok Version:1.18.20
- springfox-swagger2 y springfox-swagger-ui Version:2.9.2

## ¿Cómo iniciar la Aplicación?

Debería poder iniciar la aplicación ejecutando com.jbc.figuras.AplicacionFigura, que inicia un servidor web en el puerto 8020 y sirve SwaggerUI (http://localhost:8020/swagger-ui.html) donde puede inspeccionar y probar los puntos finales existentes.

## Endpoints de la API Rest Figuras

- GET /figuras
- POST /figuras
- GET /figuras/{tipo}
- GET /figuras/{tipo}/{id}

## Ejemplo de Prueba de Endpoints

### - Para obtener la lista de figuras invoque el siguiente endpoint con el tipo de solicitud GET

```
http://localhost:8020/figuras
```

### - Para crear una nueva figura, utilice la siguiente URL con el tipo de solicitud POST

```
http://localhost:8020/figuras
```
Establecer el Content-Type de la cabecera como application/json y el cuerpo de la solicitud como raw con JSON.

```
{
  "altura": 4,
  "base": 4,
  "radio": 0,
  "tipoFigura": "Cuadrado"
}
```

### - Para obtener las figuras de un tipo en particular, utilice la siguiente URL con el tipo de solicitud GET

```
http://localhost:8020/figuras/<tipo>

Ejemplo:
http://localhost:8020/figuras/cuadrado
```

### - Para obtener una figura en particular de un determinado tipo, utilice la siguiente URL con el tipo de solicitud GET

```
http://localhost:8020/figuras/<tipo>/<id>

Ejemplo:
http://localhost:8020/figuras/cuadrado/1
```