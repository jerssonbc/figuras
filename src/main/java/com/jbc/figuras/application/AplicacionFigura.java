/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jbc.figuras.application;

import com.jbc.figuras.dto.FiguraRequestDto;
import com.jbc.figuras.dto.FiguraResponseDto;
import com.jbc.figuras.entities.Cuadrado;
import com.jbc.figuras.entities.Triangulo;
import com.jbc.figuras.entities.Circulo;
import com.jbc.figuras.entities.Figura;
import com.jbc.figuras.repository.CirculoRepository;
import com.jbc.figuras.repository.CuadradoRepository;
import com.jbc.figuras.repository.TrianguloRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jersson
 */
@Repository
public class AplicacionFigura {

    @Autowired
    CuadradoRepository cuadradoRepository;
    @Autowired
    TrianguloRepository trianguloRepository;
    @Autowired
    CirculoRepository circuloRepository;

    public FiguraResponseDto registrarFigura(FiguraRequestDto figuraRequestDto) {
        FiguraResponseDto figuraResponseDto = null;
        switch (figuraRequestDto.getTipoFigura().toUpperCase()) {
            case "CUADRADO":
                Cuadrado cuadrado = new Cuadrado(figuraRequestDto.getBase(),
                        figuraRequestDto.getAltura());
                cuadrado = cuadradoRepository.save(cuadrado);
                figuraResponseDto = obtenerFiguraResponse(cuadrado);
                break;
            case "TRIANGULO":
                Triangulo triangulo = new Triangulo(figuraRequestDto.getBase(),
                        figuraRequestDto.getAltura());
                triangulo = trianguloRepository.save(triangulo);
                figuraResponseDto = obtenerFiguraResponse(triangulo);
                break;
            case "CIRCULO":
                Circulo circulo = new Circulo(figuraRequestDto.getRadio());
                circulo = circuloRepository.save(circulo);
                figuraResponseDto = obtenerFiguraResponse(circulo);
                break;
        }
        return figuraResponseDto;
    }

    private FiguraResponseDto obtenerFiguraResponse(Figura figura) {
        FiguraResponseDto figuraResponseDto = new FiguraResponseDto();
        figuraResponseDto.setTipoFigura(figura.getTipoFigura());

        if (figura instanceof Circulo) {
            figuraResponseDto.setId(((Circulo) figura).getId());
            figuraResponseDto.setDiametro(((Circulo) figura).obtenerDiametro());
            return figuraResponseDto;
        }
        if (figura instanceof Cuadrado) {
            figuraResponseDto.setId(((Cuadrado) figura).getId());
            figuraResponseDto.setBase(((Cuadrado) figura).getBase());
            figuraResponseDto.setAltura(((Cuadrado) figura).getAltura());
            figuraResponseDto.setSuperficie(((Cuadrado) figura).obtenerSuperficie());
            return figuraResponseDto;
        }
        figuraResponseDto.setId(((Triangulo) figura).getId());
        figuraResponseDto.setBase(((Triangulo) figura).getBase());
        figuraResponseDto.setAltura(((Triangulo) figura).getAltura());
        figuraResponseDto.setSuperficie(((Triangulo) figura).obtenerSuperficie());
        return figuraResponseDto;
    }

    public List<FiguraResponseDto> obtenerFiguras() {
        List<FiguraResponseDto> figuras = obtenerCuadrados();
        figuras.addAll(obtenerTriangulos());
        figuras.addAll(obtenerCirculos());
        return figuras;
    }

    private List<FiguraResponseDto> obtenerCuadrados() {
        List<FiguraResponseDto> figuras = new ArrayList<>();
        List<Cuadrado> cuadrados = cuadradoRepository.findAll();
        cuadrados.forEach(cuadrado -> {
            FiguraResponseDto figuraResponseDto = new FiguraResponseDto();
            figuraResponseDto.setId(cuadrado.getId());
            figuraResponseDto.setBase(cuadrado.getBase());
            figuraResponseDto.setAltura(cuadrado.getAltura());
            figuraResponseDto.setSuperficie(cuadrado.obtenerSuperficie());
            figuraResponseDto.setTipoFigura(cuadrado.getTipoFigura());
            figuras.add(figuraResponseDto);
        });
        return figuras;
    }

    private List<FiguraResponseDto> obtenerTriangulos() {
        List<FiguraResponseDto> figuras = new ArrayList<>();
        List<Triangulo> triangulos = trianguloRepository.findAll();
        triangulos.forEach(triangulo -> {
            FiguraResponseDto figuraResponseDto = new FiguraResponseDto();
            figuraResponseDto.setId(triangulo.getId());
            figuraResponseDto.setBase(triangulo.getBase());
            figuraResponseDto.setAltura(triangulo.getAltura());
            figuraResponseDto.setSuperficie(triangulo.obtenerSuperficie());
            figuraResponseDto.setTipoFigura(triangulo.getTipoFigura());
            figuras.add(figuraResponseDto);
        });
        return figuras;
    }

    private List<FiguraResponseDto> obtenerCirculos() {
        List<FiguraResponseDto> figuras = new ArrayList<>();
        List<Circulo> triangulos = circuloRepository.findAll();
        triangulos.forEach(circulo -> {
            FiguraResponseDto figuraResponseDto = new FiguraResponseDto();
            figuraResponseDto.setId(circulo.getId());
            figuraResponseDto.setTipoFigura(circulo.getTipoFigura());
            figuraResponseDto.setDiametro(circulo.obtenerDiametro());
            figuras.add(figuraResponseDto);
        });
        return figuras;
    }

    public List<FiguraResponseDto> obtenerFigurasPorTipo(String tipoFigura) {
        List<FiguraResponseDto> figuras = null;
        switch (tipoFigura.toUpperCase()) {
            case "CUADRADO":
                figuras = obtenerCuadrados();
                break;
            case "TRIANGULO":
                figuras = obtenerTriangulos();
                break;
            case "CIRCULO":
                figuras = obtenerCirculos();
                break;
        }
        return figuras;
    }

    public FiguraResponseDto obtenerFigurasPorTipoyId(String tipoFigura, long id) {
        FiguraResponseDto figuraResponseDto = null;
        switch (tipoFigura.toUpperCase()) {
            case "CUADRADO":
                if (cuadradoRepository.existsById(id)) {
                    Cuadrado cuadrado = cuadradoRepository.getById(id);
                    figuraResponseDto = obtenerFiguraResponse(cuadrado);
                }
                break;
            case "TRIANGULO":
                if (trianguloRepository.existsById(id)) {
                    Triangulo triangulo = trianguloRepository.getById(id);
                    figuraResponseDto = obtenerFiguraResponse(triangulo);
                }
                break;
            case "CIRCULO":
                if (circuloRepository.existsById(id)) {
                    Circulo circulo = circuloRepository.getById(id);
                    figuraResponseDto = obtenerFiguraResponse(circulo);
                }
                break;
        }
        return figuraResponseDto;
    }
}
