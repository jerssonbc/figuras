/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jbc.figuras.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Data;

/**
 *
 * @author jersson
 */
@Entity
@Data
public class Circulo extends Figura implements Calculo{
    final double PI=3.1416;    

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    
    private double radio;
    
    public Circulo() {
        super("CIRCULO");
    }
    
    public Circulo(double radio) {
        super("CIRCULO");
        this.radio = radio;
    }    

    @Override
    public double obtenerSuperficie() {
        return 0;
    }

    @Override
    public double obtenerDiametro() {
         return 2*getRadio();
    }
}
