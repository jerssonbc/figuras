/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jbc.figuras.entities;

/**
 *
 * @author jersson
 */
public interface Calculo {
    double obtenerSuperficie();
    double obtenerDiametro();
}
