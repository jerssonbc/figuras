/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jbc.figuras.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Data;

/**
 *
 * @author jersson
 */
@Entity
@Data
public class Cuadrado extends Figura implements Calculo{
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;    
    private double base;
    private double altura;

    public Cuadrado() {
        super("CUADRADO");
    }
    
    public Cuadrado(double base, double altura) {
        super("CUADRADO");
        this.base = base;
        this.altura = altura;
    }   

    @Override
    public double obtenerSuperficie() {
        return getBase()*getAltura();
    }

    @Override
    public double obtenerDiametro() {
        return 0;
    }
    
}
