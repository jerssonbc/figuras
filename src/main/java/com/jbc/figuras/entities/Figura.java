/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jbc.figuras.entities;

import lombok.Data;

/**
 *
 * @author jersson
 */

@Data
public abstract class Figura {    
    private String tipoFigura;

    public Figura() {
    }
    public Figura(String tipoFigura) {
        this.tipoFigura = tipoFigura;
    }    
}
