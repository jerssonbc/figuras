/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jbc.figuras.repository;

import com.jbc.figuras.entities.Circulo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author jersson
 */
public interface CirculoRepository extends JpaRepository<Circulo, Long> {
    
}
