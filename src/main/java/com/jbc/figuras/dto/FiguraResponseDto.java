/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jbc.figuras.dto;

import lombok.Data;

/**
 *
 * @author jersson
 */
@Data
public class FiguraResponseDto {
    private long id;
    private double superficie;
    private double base;
    private double altura;
    private double diametro;
    private String tipoFigura;
}
