/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jbc.figuras.controller;

import com.jbc.figuras.application.AplicacionFigura;
import com.jbc.figuras.dto.FiguraRequestDto;
import com.jbc.figuras.dto.FiguraResponseDto;
import com.jbc.figuras.validator.RespuestaValidacion;
import com.jbc.figuras.validator.ValidadorFigura;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author jersson
 */
@RestController
@RequestMapping("/figuras")
public class FiguraController {

    @Autowired
    AplicacionFigura aplicacionFigura;
    @Autowired
    ValidadorFigura validadorFigura;

    @GetMapping()
    public List<FiguraResponseDto> list() {
        return aplicacionFigura.obtenerFiguras();
    }

    @GetMapping("/{tipo}")
    public ResponseEntity<?> getByType(@PathVariable String tipo) {
        RespuestaValidacion respuestaValidacion = validadorFigura.validarTipoFigura(tipo);
        if (respuestaValidacion.isEsValido()) {
            return ResponseEntity.ok(aplicacionFigura.obtenerFigurasPorTipo(tipo));
        }else{
             return ResponseEntity.badRequest().body(respuestaValidacion);
        }
    }
    @GetMapping("/{tipo}/{id}")
    public ResponseEntity<?> getByTypeAndId(@PathVariable String tipo, @PathVariable long id) {
        RespuestaValidacion respuestaValidacion = validadorFigura.validarTipoFigura(tipo);
        if (respuestaValidacion.isEsValido()) {
            FiguraResponseDto figuraResponseDto = aplicacionFigura.obtenerFigurasPorTipoyId(tipo, id);
            if(figuraResponseDto != null){
                return ResponseEntity.ok(figuraResponseDto);
            }else{
                return new ResponseEntity<String>(HttpStatus.NO_CONTENT);                
            }            
        }else{
            return ResponseEntity.badRequest().body(respuestaValidacion);
        }
    }
    @PostMapping
    public ResponseEntity<?> post(@RequestBody FiguraRequestDto figuraRequestDto) {
        RespuestaValidacion respuestaValidacion = validadorFigura.validarParametrosFigura(figuraRequestDto);
        if (respuestaValidacion.isEsValido()) {
            FiguraResponseDto figuraResponseDto = aplicacionFigura.registrarFigura(figuraRequestDto);
            return ResponseEntity.ok(figuraResponseDto);
        } else {
            return ResponseEntity.badRequest().body(respuestaValidacion);
        }
    }
}
