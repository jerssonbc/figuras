/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jbc.figuras.validator;

import com.jbc.figuras.dto.FiguraRequestDto;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jersson
 */
@Repository
public class ValidadorFigura {
    public RespuestaValidacion validarParametrosFigura(FiguraRequestDto figuraRequestDto) {
        RespuestaValidacion respuestaValidacion;
        switch (figuraRequestDto.getTipoFigura().toUpperCase()) {
            case "CUADRADO":
                respuestaValidacion = validaParametrosCuadradoTriangulo(figuraRequestDto);
                break;
            case "TRIANGULO":
                respuestaValidacion = validaParametrosCuadradoTriangulo(figuraRequestDto);
                break;
            case "CIRCULO":
                respuestaValidacion = validaParametrosCirculo(figuraRequestDto);
                break;
            default:
                respuestaValidacion = new RespuestaValidacion();
                respuestaValidacion.setEsValido(false);
                respuestaValidacion.setMensaje("Ingrese tipo figura valida(Cuadrado,Triangulo,Circulo)");
                break;
        }
        return respuestaValidacion;
    }
    private RespuestaValidacion validaParametrosCuadradoTriangulo(FiguraRequestDto figuraRequestDto) {
        RespuestaValidacion respuestaValidacion = new RespuestaValidacion();
        respuestaValidacion.setEsValido(true);
        String mensaje = "";       
        if (figuraRequestDto.getBase() <= 0) {
            respuestaValidacion.setEsValido(false);
            mensaje += "Ingrese un valor mayor que cero para la base.";
        }
        if (figuraRequestDto.getAltura() <= 0) {
            respuestaValidacion.setEsValido(false);
            mensaje += "Ingrese un valor mayor que cero para la altura.";
        }
        respuestaValidacion.setMensaje(mensaje);
        return respuestaValidacion;
    }
    private RespuestaValidacion validaParametrosCirculo(FiguraRequestDto figuraRequestDto){
        RespuestaValidacion respuestaValidacion = new RespuestaValidacion();
        respuestaValidacion.setEsValido(true);
        String mensaje = "";
        if (figuraRequestDto.getRadio() <= 0) {
            respuestaValidacion.setEsValido(false);
            mensaje += "Ingrese un valor mayor que cero para el radio.";
        }
        respuestaValidacion.setMensaje(mensaje);
        return respuestaValidacion;
    }
    public RespuestaValidacion validarTipoFigura(String tipoFigura){
        RespuestaValidacion respuestaValidacion = new RespuestaValidacion();
        respuestaValidacion.setEsValido(true);
        switch (tipoFigura.toUpperCase()) {
            case "CUADRADO":
            case "TRIANGULO":
            case "CIRCULO":                
                break;
            default:                
                respuestaValidacion.setEsValido(false);
                respuestaValidacion.setMensaje("Ingrese tipo figura valida(Cuadrado,Triangulo,Circulo)");
                break;
        }
        return respuestaValidacion;
    }
}
