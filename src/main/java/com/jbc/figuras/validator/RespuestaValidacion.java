/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jbc.figuras.validator;

import lombok.Data;

/**
 *
 * @author jersson
 */
@Data
public class RespuestaValidacion {
    private boolean esValido;
    private String Mensaje;
}
